#include "vector.h"

Vector::Vector(int n)
{
	if (n < MIN_VALUE) {
		n = MIN_VALUE; // changing n to 2
	}
	// initializing fields
	this->_elements = new int[n]; // allocating memory
	this->_size = 0;
	this->_capacity = n;
	this->_resizeFactor = n;
}

Vector::~Vector()
{
	delete[] _elements;
	_elements = nullptr;
}

int Vector::size() const
{
	return this->_size;
}

int Vector::capacity() const
{
	return this->_capacity;
}

int Vector::resizeFactor() const
{
	return this->_resizeFactor;
}

bool Vector::empty() const
{
	return (this->_size == 0);
}

void Vector::printVector() const
{
	for (int i = 0; i < this->_size; i++)
	{
		std::cout << this->_elements[i] << std::endl;
	}
}


void Vector::push_back(const int& val)
{
	this->resize(this->_size + 1); // increas size by 1
	this->_elements[this->_size - 1] = val; // placing value in the back
}

int Vector::pop_back()
{
	int val = -9999;
	if (this->_size) // if not empty
	{
		val = this->_elements[this->_size - 1]; // saving value being poped
		this->_elements[this->_size - 1] = 0;
		this->_size--; // decreas vector size
	}
	else
	{
		std::cout << "error: pop from empty vector" << std::endl;
	}

	return val;
}

void Vector::reserve(const int n)
{
	int* new_arr = nullptr;
	while (this->_capacity < n)
	{ // adding resize factor to capacity until greater than n
		this->_capacity += this->_resizeFactor;
	}

	new_arr = new int[this->_capacity]; // allocating new memory
	for (int i = 0; i < this->_size; i++)
	{ // copying vector values to array
		new_arr[i] = this->_elements[i];
	}

	delete[] this->_elements;
	this->_elements = new_arr; // replacing elements with new array with larger memory size
}

void Vector::resize(const int n)
{
	if (n > this->_capacity)
	{
		reserve(n);
	}
	this->_size = n;
}

void Vector::assign(const int val)
{
	for (int i = 0; i < this->_size; i++)
	{
		this->_elements[i] = val;
	}
}

void Vector::resize(const int n, const int& val)
{
	int org_size = this->_size;
	this->resize(n);

	while (org_size < this->_size)
	{ // copying val to the new elements added
		this->_elements[org_size] = val;
		org_size++;
	}
}

Vector::Vector(const Vector& other) :_elements(nullptr)
{
	*this = other; // uses copy operator - copy other object to this object
}

Vector& Vector::operator=(const Vector& other)
{
	if (this == &other) // tries to copy the object to itself
	{
		return *this;
	}

	// copying fields
	this->_capacity = other._capacity;
	this->_resizeFactor = other._resizeFactor;
	this->_size = other._size;

	delete[] this->_elements;
	this->_elements = new int[other._capacity];
	for (int i = 0; i < other._size; i++)
	{ // copying elements
		this->_elements[i] = other._elements[i];
	}

	return *this;
}

int& Vector::operator[](int n) const
{
	if (n >= this->_size || n < 0)
	{
		std::cout << "Illegal index" << std::endl;
		n = 0;
	}

	return this->_elements[n];
}
